﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HexGrid : MonoBehaviour {
    public static HexGrid Instance { get; set; }
    public HexCell cellPrefab;
    public GameObject highlinePrefab;
    /*
    public Color firstColor;
    public Color secondColor;
    public Color thirdColor;
    public Color touchedColor = Color.magenta;*/

    public GameObject firstIsland;
    public GameObject secondIsland;
    public GameObject thirdIsland;

    private GameObject islandsParent;

    private BoardManager board;

    public Color attackColor;
    public Color attackColor2;
    public Color moveColor;
    public Color moveColor2;
    public Color arrowColor;
    public Color arrowColor2;
    public Color activeColor;
    public Color activeColor2;

    [SerializeField]
    private GameObject messageWindow;
    private MessageWindow _messageWindow;

    public enum MoveType { Kill, Move, Arrow };

    private Dictionary<HexCell, MoveType> _possibleMoves;
    private Dictionary<MoveType, Color> _moveColors1;
    private Dictionary<MoveType, Color> _moveColors2;

    private Dictionary<Vector3, Vector3> enpassanDict;

    private GameObject _parentCells;
    private HexCell _active_cell = null;

    public static HexCell[] cells;
    private Dictionary<HexCell, Highline> highlines;

    private IslandShaker shiverIsland;

    public GameObject[] islands;

    public Text cellLabelPrefab;
    [SerializeField]
    private Server server;
    Canvas gridCanvas;

    HexMesh hexMesh;

    public static int cellsCount;
    public static int coordsLength;
    public static int boardSize;

    List<CellCoords> cellCoordsList = new List<CellCoords>();

    Coroutine shiverCoroutine;

    class CellCoords {
        int cellIndex;
        Vector3 position;

        public CellCoords(int index, Vector3 coords) {
            cellIndex = index;
            position = coords;
        }

        public bool IsInside(Vector3 coords) {
            float xPos = position.x;
            float yPos = position.y;
            float zPos = position.z;

            float r = HexMetrics.innerRadius;

            if (Vector3.Distance(position, coords) <= r)
                return true;
            return false;
        }
    }

    public void SetEnpassanMove(Vector3 point1, Vector3 point2)
    {
        enpassanDict.Add(point1, point2);
    }

    void DrawBoard(int size) {
        boardSize = size;
        coordsLength = size - 1;
        cellsCount = 6 * (size / 2) * (size - 1) + 1;
        _parentCells = new GameObject();
        _parentCells.name = "Cells";
        _parentCells.transform.parent = this.transform;
        gridCanvas = GetComponentInChildren<Canvas>();
        hexMesh = GetComponentInChildren<HexMesh>();
        //обработчик чуть выше самой доски
        hexMesh.transform.position += Vector3.up * 1f;

        cells = new HexCell[cellsCount];
        highlines = new Dictionary<HexCell, Highline>();
        islands = new GameObject[cellsCount];

    int currentEl = 0;

        int rowsCount = 1 + 4 * (size - 1); //for example rows from 0 to 19 for size = 6
        int currentRow = rowsCount;

        int topRowsCount = size - 1;
        int bottomRowsCount = topRowsCount;
        int middleRowsCount = size * 2 - 1;

        DrawTopRows(topRowsCount, ref currentEl, ref currentRow);
        DrawMiddleRows(middleRowsCount, ref currentEl, ref currentRow);
        DrawBottomRows(bottomRowsCount, ref currentEl, ref currentRow);
    }

    void DrawTopRows(int rowsCount, ref int currentEl, ref int currentRow) {
        int startZ = 0;
        int startX = -coordsLength;
        int startY = coordsLength;

        for (int rowWidth = 0; rowWidth < rowsCount; rowWidth++) {
            int currentX = startX;
            int currentZ = startZ;
            int currentY = startY;

            for (int currentWidth = 0; currentWidth <= rowWidth; currentWidth++) {
                CreateCell(currentX, currentY, currentZ, currentEl++, currentRow);
                currentZ -= 2;
                currentX++;
                currentY++;
            }
            currentRow--;
            startY--;
            startZ++;
        }
    }

    void DrawMiddleRows(int rowsCount, ref int currentEl, ref int currentRow) {
        int startZ = coordsLength;
        int startY = 0;
        int startX = -coordsLength;

        for (int rowNum = 0; rowNum < rowsCount; rowNum += 2) {
            int currentX = startX;
            int currentZ = startZ;
            int currentY = startY;

            for (int currentWidth = 0; currentWidth < boardSize; currentWidth++) {
                CreateCell(currentX++, currentY++, currentZ, currentEl++, currentRow);
                currentZ -= 2;
            }

            currentRow--;
            currentZ = startZ - 1;
            currentX = startX + 1;
            currentY = startY;

            for (int currentWidth = 0; currentWidth < boardSize - 1; currentWidth++) {
                CreateCell(currentX++, currentY++, currentZ, currentEl++, currentRow);
                currentZ -= 2;
            }
            currentRow--;

            startY--;
            startX++;
        }
    }

    void DrawBottomRows(int rowsCount, ref int currentEl, ref int currentRow) {
        int startZ = coordsLength - 2;
        int startY = -coordsLength;
        int startX = 2;

        for (int rowNum = 0; rowNum < rowsCount; rowNum++) {
            int currentX = startX;
            int currentZ = startZ;
            int currentY = startY;

            for (int currentWidth = 0; currentWidth < rowsCount - rowNum - 1; currentWidth++) {
                CreateCell(currentX, currentY, currentZ, currentEl++, currentRow);
                currentZ -= 2;
                currentX++;
                currentY++;
            }

            currentRow--;
            startZ--;
            startX++;
        }
    }


    void Awake() {
        _messageWindow = messageWindow.GetComponent<MessageWindow>();
        Instance = this;
        enpassanDict = new Dictionary<Vector3, Vector3>();
        _moveColors1 = new Dictionary<MoveType, Color> {
            { MoveType.Kill, attackColor },
            { MoveType.Move, moveColor},
            { MoveType.Arrow, arrowColor}
        };

        _moveColors2 = new Dictionary<MoveType, Color> {
            { MoveType.Kill, attackColor2 },
            { MoveType.Move, moveColor2},
            { MoveType.Arrow, arrowColor2}
        };

        _possibleMoves = null;
        islandsParent = new GameObject();
        islandsParent.name = "Islands";
        islandsParent.transform.parent = transform;
        DrawBoard(6);
        BoardManager.Instance.SetCenterPoint(CenterCoords());
    }

    void Start() {
        hexMesh.Triangulate(cells);
        board = BoardManager.Instance;
    }

    public void DiscardSelection() {
        if (shiverIsland!= null) shiverIsland.StopShivering();
        shiverIsland = null;
        if (highlines.Count != 0)
            foreach(var highline in highlines) {
                highline.Value.Fade();
            }
        if (_possibleMoves != null) {
            if (_active_cell != null) { _active_cell.UndoSelection(); _active_cell = null; }
            if (_possibleMoves.Count > 0) {
                foreach (var selectedCell in _possibleMoves) {
                    selectedCell.Key.UndoSelection();
                    highlines[selectedCell.Key].Fade(); 
                }
            };
            _possibleMoves = null;
            hexMesh.Triangulate(cells);
        }
    }

    //сервер переставляет фигурки:
    public void ActivateChessman(MoveType action, Vector3 startCoords, Vector3 finalCoords) {
        
        var board = BoardManager.Instance;
        HexCell startCell = board.GetCellByCoords((int)startCoords.x, (int)startCoords.y, (int)startCoords.z);
        HexCell finalCell = board.GetCellByCoords((int)finalCoords.x, (int)finalCoords.y, (int)finalCoords.z);
        Player owner;

        GameObject chessman = startCell.GetChessman();
        //Debug.Log("chessman = " + chessman);
        //Debug.Log("chessman.GetComponentsInChildren<Movable>() = " + chessman.GetComponentsInChildren<Movable>());
        if(chessman == null) {
            return;
        }
        Movable[] movableScripts = chessman.GetComponentsInChildren<Movable>();
        foreach (var script in movableScripts) {
            script.SignFirstMove();
        }

        switch (action) {
            case MoveType.Move:
                Chessman chessmanScript = chessman.GetComponentInChildren<Chessman>();
                owner = chessmanScript.ChessPlayer;
                if (board.IsKing(chessmanScript)) board.kings[owner] = finalCell;
                finalCell.SpawnChessman(startCell.GetChessman(), owner);
                startCell.RemoveChessman();

                break;
            case MoveType.Kill:
                //CHECK: if final cell has a king - current player wins! 
                foreach (var entry in board.kings)
                    if (entry.Value == finalCell) Debug.Log("WIN");
                owner = startCell.GetChessman().GetComponentInChildren<Chessman>().ChessPlayer;

                finalCell.RemoveChessman();
                finalCell.SpawnChessman(startCell.GetChessman(), owner);
                startCell.RemoveChessman();
                break;
            default:
                break;
        }

        board.NextPlayerTurn();
    }

    public StandartMove TryMove(MoveType action, HexCell startCell, HexCell finalCell)
    {
        Player owner;

        //берём фигуру со стартового поля
        GameObject chessman = startCell.GetChessman();
        //берём все возможные ходы
        Movable[] movableScripts = chessman.GetComponentsInChildren<Movable>();
        foreach (var script in movableScripts)
        {
            script.SignFirstMove();
        }

        //нам нужно найти абилку именно мув акшн
        StandartMove moveAction = null;
        Player enemy = GetEnemy();
        foreach (var hexAbility in finalCell.hexAbilities)
        {
            if (hexAbility.GetType() == typeof(StandartMove))
            {
                moveAction = hexAbility as StandartMove;
            }
        }

        Chessman chessmanScript = chessman.GetComponentInChildren<Chessman>();
        owner = chessmanScript.ChessPlayer;

        switch (action)
        {
            //если это перемещение
            case MoveType.Move:

                //смотрим де король текущего игрока
                if (board.IsKing(chessmanScript)) board.kings[owner] = finalCell;
                //Debug.Log("Move " + chessman.GetComponent<Movable>().GetType().ToString());

                //меняем родителя фигуры логически, но не визуально
                if (moveAction != null)
                    moveAction.ChangeParent(startCell, finalCell.transform);
                else
                    Debug.Log("moveaction = null");

                break;
            case MoveType.Kill:
                //CHECK: if final cell has a king - current player wins!
                foreach (var entry in board.kings)
                    if (entry.Value == finalCell) Debug.Log("WIN");
                if (board.IsKing(chessmanScript)) board.kings[owner] = finalCell;
                //Debug.Log("Kill by " + chessman.GetComponent<Movable>().GetType().ToString());
                if (moveAction != null)
                    moveAction.ChangeParent(startCell, finalCell.transform);
                else
                    Debug.Log("moveaction = null");

                break;
            case MoveType.Arrow:
                foreach (var entry in board.kings)
                    if (entry.Value == finalCell) Debug.Log("WIN");

                owner = startCell.GetChessman().GetComponentInChildren<Chessman>().ChessPlayer;

                finalCell.RemoveChessman();
                GameManager.Instance.AbilityIsReady();
                break;
            default:
                break;
        }

        return moveAction;
    }

    //вызывается, когда игрок кликает на подсвеченную клетку
    public void TryActivateChessman(MoveType action, HexCell startCell, HexCell finalCell) {
        GameObject chessman = startCell.GetChessman();
        StandartMove moveAction = TryMove(action, startCell, finalCell);

        GameManager gm = GameManager.Instance;

        bool moved = false;

        //начинаем с нуля?
        int fullAbilitiesCount = 0;
        if (IsCheck(GetEnemy(), GetAlly())) {
            //не надо делать ход
            gm.RevertTurn();
            gm.SetActiveAbilities(0);
            moveAction.ReturnParent();
            _messageWindow.ShowChangeTurn();
            Chessman chessmanScript = chessman.GetComponentInChildren<Chessman>();
            if (board.IsKing(chessmanScript)) board.kings[chessmanScript.ChessPlayer] = startCell;
            return;
        }
        
        //считаем скоко всего действий надо сделать
        foreach (var hexAbility in finalCell.hexAbilities) {
            if (hexAbility.WorksFor(chessman)) fullAbilitiesCount++;
        };

        gm.SetActiveAbilities(fullAbilitiesCount);

        moveAction.ReturnParent();
        if (action == MoveType.Kill) finalCell.RemoveChessman();
        foreach (var hexAbility in finalCell.hexAbilities) {
            if (hexAbility.WorksFor(chessman)) hexAbility.Move(startCell, chessman.GetComponentInChildren<Chessman>(), finalCell);
        }

        //если мы всё-таки походили, нужно делать проверки для взятия на проходе
        //если пешка походила на два вперёд - отметим это в её скрипте
        //если пешка НЕ походила - соотвественно enpassan в её скрипте приравниваем к false
        //если пешка походила на один из возможных "энпассан" ходов - убрать пешку, которую она убила
        if (chessman.GetComponent<Pawn>() != null)
        {
            
        }

        if (moved) GameManager.Instance.AbilityIsReady();
        int moveType = (int) action;
        Vector3 start = new Vector3(startCell.X, startCell.Y, startCell.Z);
        Vector3 final = new Vector3(finalCell.X, finalCell.Y, finalCell.Z);
        server.CmdStep(moveType, start, final);
    }

    public Player GetEnemy() {
        if (board.CurrentPlayer == board.GetFirstPlayer()) {
            return board.GetSecondPlayer();
        }
        else {
            return board.GetFirstPlayer();
        }
    }

    public Player GetAlly() {
        if (board.CurrentPlayer == board.GetFirstPlayer()) {
            return board.GetFirstPlayer();
        }
        else {
            return board.GetSecondPlayer();
        }
    }

    public void ActivateHighLine(HexCell cell, Color color1, Color color2, bool isActive) {
        Highline highline = highlines[cell];
        highline.ChangeColors(color1, color2);
        highline.Appear();
    }

    public void ClickCell(Vector3 position) {

        HexCell cell = FindCell(position);

        if (cell == null) return;

        //if position corresponds to one of selected cell (one of possible moves) - do something (move/kill/etc) with the chessman!
        //and then discard selection
        if (_possibleMoves != null && _active_cell != null)
            foreach (var currentCell in _possibleMoves) {
                HexCell cellCoords = currentCell.Key;
                if (cell.X == cellCoords.X && cell.Y == cellCoords.Y && cell.Z == cellCoords.Z) {
                    TryActivateChessman(currentCell.Value, _active_cell, cell);
                    DiscardSelection();
                    return;
                }
            }

        //clear previous selection
        enpassanDict = new Dictionary<Vector3, Vector3>();
        DiscardSelection();

        if (cell.HasChessman()) {
            if (cell.GetChessman().GetComponentInChildren<Chessman>().ChessPlayer != BoardManager.Instance.CurrentPlayer) return;
            _active_cell = cell;
            cell.SetSelectionColor(activeColor);
            ActivateHighLine(cell, activeColor, activeColor2, true);
            ShakeIsland(highlines[cell].transform.parent.gameObject, cell.GetChessman());
        }
        else return;

        _possibleMoves = cell.GetChessmanMoves();

        //if we have at least one move for those chessman
        if (_possibleMoves != null && _possibleMoves.Count > 0)
            //each Cell to select and type of type of movement
            foreach (var possibleCell in _possibleMoves) {
                Color selectionColor1;
                Color selectionColor2;
                //we want to find color of this type of movement to select our cell
                bool found1 = _moveColors1.TryGetValue(possibleCell.Value, out selectionColor1);
                bool found2 = _moveColors2.TryGetValue(possibleCell.Value, out selectionColor2);
                if (found1 && found2)
                {
                    possibleCell.Key.SetSelectionColor(selectionColor1);
                    ActivateHighLine(possibleCell.Key, selectionColor1, selectionColor2, false);
                }

                else
                    Debug.LogError("You haven't set color for " + possibleCell.Value);
        }
        hexMesh.Triangulate(cells);
    }

    public HexCell FindCell(Vector3 currentCoords) {
        for (int i = 0; i < cellsCount; i++) {
            if (cellCoordsList[i].IsInside(currentCoords))
                return cells[i];
        }
        return null;
    }

    public HexCell FindCell(int x, int y, int z) {
        for (int i=0; i < cells.Length; i++) {
            HexCell curentCell = cells[i];
            if ((curentCell.X == x) && (curentCell.Y == y) && (curentCell.Z == z)) {
                return curentCell;
            }
        }
        return null;
    }

    private Vector3 CenterCoords() {
        int middleIndex = boardSize - 1;
        HexCell centerCell = FindCell(middleIndex, middleIndex, middleIndex);
        Transform placement = centerCell.transform;
        return new Vector3(placement.position.x, placement.position.y, placement.position.z);
    }

    private void CreateCell(int x, int y, int z, int i, int currentHeight) {
        Vector3 position;

        position.y = 0f;
        position.z = HexMetrics.innerRadius * currentHeight;
        position.x = - z * 1.5f * HexMetrics.outerRadius;

        cells[i] = Instantiate<HexCell>(cellPrefab);
        HexCell cell = cells[i];
        cell.transform.SetParent(_parentCells.transform, false);
        cell.transform.localPosition = position;

        cellCoordsList.Add(new CellCoords(i, cell.transform.position));

        GameObject island;
        if (currentHeight % 3 == 0) {
            //cell.SetNativeColor(firstColor);
            island = Instantiate<GameObject>(firstIsland);
        }
        else if (currentHeight % 3 == 1) {
            //cell.SetNativeColor(secondColor);
            island = Instantiate<GameObject>(secondIsland);
        }
        else {
            //cell.SetNativeColor(thirdColor);
            island = Instantiate<GameObject>(thirdIsland);
        }

        cell.X = x;
        cell.Y = y;
        cell.Z = z;

        island.AddComponent<IslandShaker>();

        GameObject highline;
        highline = Instantiate(highlinePrefab);
        highline.transform.position = position + new Vector3(0f, -8f, 0f);
        float yOffset = UnityEngine.Random.Range(0f, 6f);
        cell.yOffset = yOffset;
        island.transform.position = position - new Vector3(0f, 8f + yOffset, 0f);
        highline.transform.position -= new Vector3(0f, yOffset, 0f);
        island.transform.SetParent(islandsParent.transform);
        highline.transform.SetParent(island.transform);
        islands[i] = island;
        Highline highlineScript = highline.GetComponent<Highline>();
        highlines.Add(cell, highlineScript);
        highlineScript.Fade(0.1f);
        
        Text label = Instantiate<Text>(cellLabelPrefab);
        label.rectTransform.SetParent(gridCanvas.transform, false);
        label.rectTransform.anchoredPosition =
            new Vector3(position.x, position.y, position.z);

        label.text = (cell.X.ToString() + " " + cell.Y.ToString() + " " + cell.Z.ToString());
        label.transform.position = position;     

    }

    public void AfterTurn() {
        //текущий пользователь наш парень, а второй - противник
        Player ally;
        Player enemy;
        if (board.CurrentPlayer == board.GetFirstPlayer()) {
            ally = board.GetFirstPlayer();
            enemy = board.GetSecondPlayer();
        }
        else {
            ally = board.GetSecondPlayer();
            enemy = board.GetFirstPlayer();
        }
        bool isCheck = IsCheck(enemy, ally);
        if (isCheck)
        {
            //Debug.Log("we're in check");
            if (isMate()) Debug.LogError("MATE!!!");
            else
            {
                _messageWindow.ShowKingAttacked();
                //Debug.LogError("King is attacked!");
                //Debug.LogError("King is attacked on position: " + board.kings[board.CurrentPlayer].X + " " + board.kings[board.CurrentPlayer].Y + " " + board.kings[board.CurrentPlayer].Z);
            }
        }
    }

    public void ShakeIsland(GameObject island, GameObject chessman) {
        
        if (shiverIsland != null) shiverIsland.StopShivering();
        shiverIsland = island.GetComponent<IslandShaker>();
        //
        shiverIsland.StartShivering(chessman);
    }

    //фигура логически не переставляется для врага?

    //чтобы узнать, грозит ли королю мат, мы типо должны взять атакующие ходы всех вражеских фигур и удостовериться
    //что ни одна из них не совпадает с позицией короля текущего игрока
    public bool IsCheck(Player enemy, Player ally) {
        //взять список вражеских фигур
        //и проверять каждую на угрозу королю
        //если это начало твоего хода - ты должен спасти короля
        //если конец - ты не должен двигать фигуру
        //Debug.Log("CHECK. Enemy: " + enemy.PlayerName + " ally: " + ally.PlayerName);

        //Debug.Log(board.CurrentPlayer);
        var currentPlayerCell = board.kings[ally];
        //Debug.LogError("King is on  " + " " + currentPlayerCell.X + " " + currentPlayerCell.Y + " " + currentPlayerCell.Z);
        //позицию короля определяет правильно
        //враг определён правильно
        foreach (var cell in cells) {
            //для каждой клетки берём объект "фигура"
            GameObject chessmanGO = cell.GetChessman();
            //если там стоит фигура
            if (chessmanGO != null) {
                //смотрим какая
                Chessman chessman = chessmanGO.GetComponentInChildren<Chessman>();
                //Debug.Log("CHECK " + chessmanGO.GetComponent<Movable>().GetType().ToString());
                //если вражеская
                if (chessman.ChessPlayer == enemy) //it's the enemy!
                {
                    //берём все её возможные хода
                    Movable[] moves = chessmanGO.GetComponentsInChildren<Movable>();
                   
                    foreach (var move in moves) {
                        //Debug.Log("moves count = " + move.GetPossibleMoves(cell.X, cell.Y, cell.Z, HexCell.sizeLimit, enemy).Count);
                        if (chessman.ChessPlayer == enemy) //it's the enemy!
                        //передаём enemy, ибо мы смотрим возможные ходы врага
                        foreach (var possibleCell in move.GetPossibleMoves(cell.X, cell.Y, cell.Z, HexCell.sizeLimit, enemy)) {
                            if (possibleCell.Value == MoveType.Kill) {
                                    //Debug.Log("Is it kill ? currentPlayerCell = " + currentPlayerCell.X + " " + currentPlayerCell.Y + " " + currentPlayerCell.Z);
                                    //Debug.Log("Is it kill ? possibleCell = " + possibleCell.Key.X + " " + possibleCell.Key.Y + " " + possibleCell.Key.Z);
                                    if (currentPlayerCell.X == possibleCell.Key.X && currentPlayerCell.Y == possibleCell.Key.Y && currentPlayerCell.Z == possibleCell.Key.Z) {
                                    //Debug.LogError("King is attacked by  " + chessmanGO.name + " " + cell.X  + " " + cell.Y  + " " + cell.Z);
                                    //Debug.LogError("King is on  " + " " + currentPlayerCell.X + " " + currentPlayerCell.Y + " " + currentPlayerCell.Z);
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    //mate вызывается если обнаружит шах
    //мы должны для всех возможных ходов короля проверить есть ли атака на короля
    //если да - это НЕ МАТ
    //если нет:
    //проверить для всех фигур... куда они могут походить, и будет ли после этого король под атакой???7
    //ебать
    //нахуй
    //господи милосердный
    public bool isMate()
    {
        //нужно переставлять и короля, ибо board.kings[board.CurrentPlayer] возвращает ноль :\
        GameManager gm = GameManager.Instance;
        bool noMate = false;
        HexCell currentCell = board.kings[board.CurrentPlayer];
        //Debug.Log("does current chess contain a chessman? " + currentCell.GetChessman());
        //Debug.Log("does current chess contain a movable? " + currentCell.GetChessman().GetComponent<Movable>());
        King ourKing = currentCell.GetChessman().GetComponentInChildren<Movable>() as King;
        foreach (var possibleMove in ourKing.GetPossibleMoves(currentCell.X, currentCell.Y, currentCell.Z, HexCell.sizeLimit, board.CurrentPlayer))
        {
            //передвигаем короля и смотрем чо будет
            StandartMove moveAction = TryMove(possibleMove.Value, currentCell, possibleMove.Key);
        
            noMate = (!IsCheck(GetEnemy(), GetAlly()));
            //не надо делать ход
            gm.RevertTurn();
            gm.SetActiveAbilities(0);
            moveAction.ReturnParent();
            //вроде бы тут только король и может быть, наверное стоит убрать проверку?
            if (board.IsKing(currentCell.GetChessman().GetComponentInChildren<Chessman>())) board.kings[currentCell.GetChessman().GetComponentInChildren<Chessman>().ChessPlayer] = currentCell;
            if (noMate) return false;
        }
        //если король никуда убежать не может, смотрим, может ли его кто-нибудь "спасти"
        //переставление всех остальных фигур
        foreach(HexCell cell in cells) {
            GameObject chessman = cell.GetChessman();
            if (chessman != null) {
                Movable[] moves = chessman.GetComponentsInChildren<Movable>();
                //пробуем передвинуть только дружественные фигуры
                if (cell.GetChessman().GetComponentInChildren<Chessman>().ChessPlayer == board.CurrentPlayer)
                    foreach (var movableScript in moves)
                        foreach (var possibleMove in movableScript.GetPossibleMoves(currentCell.X, currentCell.Y, currentCell.Z, HexCell.sizeLimit, board.CurrentPlayer)) {
                            //передвигаем фигуру и смотрем чо будет
                            StandartMove moveAction = TryMove(possibleMove.Value, currentCell, possibleMove.Key);

                            noMate = (!IsCheck(GetEnemy(), GetAlly()));
                            //не надо делать ход
                            gm.RevertTurn();
                            gm.SetActiveAbilities(0);
                            moveAction.ReturnParent();
                            //Debug.Log("Try possibleCell = " + possibleMove.Key.X + " " + possibleMove.Key.Y + " " + possibleMove.Key.Z);
                            //Debug.Log("returns " + noMate);
                            if (noMate) return false;
                        }
            }
        }
    
        Debug.LogError("oh my god! that's mate!");
        return true;
    }
}