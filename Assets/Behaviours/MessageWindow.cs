﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class MessageWindow : MonoBehaviour {

    public string KingAttackedMessage = "Ваш король под атакой!";
    public string AnotherTurnMessage = "Походите так, чтобы король не был под атакой!";
    public string MateMessage = "Ваш король под матом. Ваша армия повержена.";
    public string WinMessage = "Вы победили!";

    private Animation _animation;

    public GameObject text;

    private Text _textScript;
    public float fadeTime = 2.5f;
    public float messageTime = 5f;
    private Color _color;

    // Use this for initialization
    void Start () {
        _textScript = text.GetComponent<Text>();
        _animation = GetComponentInChildren<Animation>();
        RestartWindow();
        
    }

    public void ShowKingAttacked() {
        Debug.Log("ShowKingAttacked");
        ShowWindow(KingAttackedMessage);
    }

    public void ShowChangeTurn()
    {
        Debug.Log("ShowChangeTurn");
        ShowWindow(AnotherTurnMessage);
    }

    public void ShowMate()
    {
        ShowWindow(MateMessage);
    }

    public void ShowWin()
    {
        ShowWindow(WinMessage);
    }

    public void ShowWindow(string text)
    {
        RestartWindow();
        _textScript.text = text;
        _animation.Play("TextAppearAnimation");
        StartCoroutine(StopShowWindow());
    }

    public void RestartWindow()
    {
        _animation.enabled = false;
        _color = _textScript.color;
        _color.a = 0f;
        _textScript.color = _color;
        _animation.enabled = true;
    }

    public IEnumerator StopShowWindow()
    {
        yield return new WaitForSeconds(messageTime);
        _color = _textScript.color;
        if (_color.a > 0)
            _animation.Play("TextFadeAnimation");
        yield return null;
    }
}
