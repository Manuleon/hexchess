﻿using UnityEngine;

public class HexMapEditor : MonoBehaviour {

    public HexGrid hexGrid;

    private Color activeColor;

    void Awake() {

    }

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            HandleInput();
        }
        else if (Input.GetMouseButtonDown(1)) {
            hexGrid.DiscardSelection();
        }
    }

    void HandleInput() {
        Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(inputRay, out hit)) {
            hexGrid.ClickCell(hit.point);
        }
    }
}