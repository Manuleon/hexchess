﻿using UnityEngine;
using System.Collections.Generic;

public class Pawn : Movable {

    //contains types of moves:
    //Move -- we can only move on the cell (if available)
    //Kill -- we can only attack the cell (if there is an enemy there)
    private enum TurnTypes  { Move, Kill };
    //indicates the current turn type
    private TurnTypes _turnType;
    public bool enpassan = false;

    public override Dictionary<HexCell, HexGrid.MoveType> GetPossibleMoves(int currentX, int currentY, int currentZ, int boardMaxCoord, Player allyPlayer)
    {
        base.DefineCoords(currentX, currentY, currentZ);
        base.DefineBoardSize(boardMaxCoord);
        var board = BoardManager.Instance;
        HexCell cell;
        HexGrid grid = HexGrid.Instance;

        List<ChangeIndexes> args = new List<ChangeIndexes>();

        
        //for first player
        if (board.GetFirstPlayer() == allyPlayer)
        {
            args.Add(() => { CurrentX -= 1; CurrentY += 1; _turnType = TurnTypes.Move; });
            args.Add(() => { CurrentX -= 1; CurrentZ += 1; _turnType = TurnTypes.Kill; });
            args.Add(() => { CurrentY += 1; CurrentZ -= 1; _turnType = TurnTypes.Kill; });
            //first try
            cell = board.GetCellByCoords(CurrentX, CurrentY - 1, CurrentZ + 1);
            if (cell != null)
            {
                if (cell.HasChessman())
                {
                    Pawn pawn = cell.GetChessman().GetComponent<Pawn>();
                    if (pawn != null && pawn.GetComponent<Chessman>().ChessPlayer != allyPlayer) {
                        if (!pawn.enpassan)
                        {
                            //board будет знать, если мы походили пешкой и походили на эту клетку, надо убрать пешку по данным координатам
                            //куда походили, где вражеская пешка
                            args.Add(() => { CurrentX -=1; CurrentZ+=1; _turnType = TurnTypes.Move; });
                            grid.SetEnpassanMove(new Vector3(CurrentX - 1, CurrentY, CurrentZ + 1), new Vector3(CurrentX, CurrentY - 1, CurrentZ + 1));
                        }
                    }
                }
            }
            //second try
            cell = board.GetCellByCoords(CurrentX + 1, CurrentY, CurrentZ - 1);
            if (cell != null)
            {
                if (cell.HasChessman())
                {
                    Pawn pawn = cell.GetChessman().GetComponent<Pawn>();
                    if (pawn != null && pawn.GetComponent<Chessman>().ChessPlayer != allyPlayer)
                    {
                        if (!pawn.enpassan)
                        {
                            //board будет знать, если мы походили пешкой и походили на эту клетку, надо убрать пешку по данным координатам
                            //куда походили, где вражеская пешка
                            args.Add(() => { CurrentY+=1; CurrentZ-=1; _turnType = TurnTypes.Move; });
                            grid.SetEnpassanMove(new Vector3(CurrentX, CurrentY + 1, CurrentZ - 1), new Vector3(CurrentX + 1, CurrentY, CurrentZ - 1));
                        }
                    }
                }
            }
        }
        //for another
        else
        {
            args.Add(() => { CurrentX += 1; CurrentY -= 1; _turnType = TurnTypes.Move; });
            args.Add(() => { CurrentY -= 1; CurrentZ += 1; _turnType = TurnTypes.Kill; });
            args.Add(() => { CurrentX += 1; CurrentZ -= 1; _turnType = TurnTypes.Kill; });
        }

        base.DefineIndexexChanging(args);

        Dictionary<HexCell, HexGrid.MoveType> defaultMoves = FindMoves(1, allyPlayer);
        return defaultMoves;
    }

    public Dictionary<HexCell, HexGrid.MoveType> FindMoves(int distance, Player player)
    {
        var board = BoardManager.Instance;
        int startX = CurrentX;
        int startY = CurrentY;
        int startZ = CurrentZ;
        var possibleMoves = new Dictionary<HexCell, HexGrid.MoveType>();
        HexCell cell;

        //we have 3 directions, and we need to check all of them
        CheckMoves checkMoves = changeCoords => {
            //for each direction changes 2 coords, and one is const
            CurrentX = startX;
            CurrentY = startY;
            CurrentZ = startZ;
            
            // distance to move
            int moveDist = 1;
            changeCoords();
            // we add extra move if it is the first move and turn type is move
            if (this._firstMove && _turnType == TurnTypes.Move)
                moveDist++;

            for (int i = 0; i < moveDist; i++)
            {
                //we skip first time because method has been invoked above
                if (i != 0)
                {
                    changeCoords();
                }
                if (startX > maxSize || startX < 0 || CurrentY > maxSize || CurrentY < 0 || CurrentZ < 0 || CurrentZ > maxSize) return; 
                cell = board.GetCellByCoords(CurrentX, CurrentY, CurrentZ);
                if(cell != null)
                {
                    if (cell.HasChessman())
                    {       
                            if (cell.GetChessman().GetComponent<Chessman>().ChessPlayer != player && _turnType == TurnTypes.Kill)
                            {
                            possibleMoves.Add(cell, HexGrid.MoveType.Kill);
                            
                            }
                            return;
                    }

                    else if(_turnType == TurnTypes.Move)
                         {
                             possibleMoves.Add(cell, HexGrid.MoveType.Move);
                         
                         }
                }
            }         
        };

        foreach (var fn in fnIndexChange)
        {
            checkMoves(fn);
        }

        return possibleMoves;
    }



}
