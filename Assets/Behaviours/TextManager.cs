﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TextManager : MonoBehaviour {

    [SerializeField]
    private GameObject turnText;
    private Text turnTextText;
    public Button[] transformButtons;
    public GameObject transformWindow;

    void Awake() {
        
    }

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void NextTurnText(Player player) {
        if (turnTextText == null) turnTextText = turnText.GetComponent<Text>();
        turnTextText.text = player.PlayerName + "'S TURN";
        turnTextText.color = player.TextColor;
    }

    public void ShowTransformWindow(GameObject chessman, GameObject[] tranforms) {
        
        //we're gettings chessmans.count button and use them
        string chessmanName;
        int i = 0;
        for (i=0; i < tranforms.Length; i++) {
            int index = i;
            transformButtons[i].onClick.AddListener(delegate { ActivateTransform(chessman, tranforms[index]); });
            chessmanName = tranforms[i].GetComponent<Chessman>().name;
            transformButtons[i].GetComponentInChildren < Text >().text = chessmanName;
            transformButtons[i].gameObject.SetActive(true);
        }

        for (; i < transformButtons.Length; i++) {
            transformButtons[i].gameObject.SetActive(false);
        }

        transformWindow.SetActive(true);

        //other buttons should not be active
    }

    public void ActivateTransform(GameObject chessman, GameObject newChessman) {
        try {
            chessman.transform.parent.GetComponent<HexCell>().RemoveChessman();
            chessman.transform.parent.GetComponent<HexCell>().SetCurrentChessman(null);
            BoardManager.Instance.InitializeChesses(chessman.transform.parent.GetComponent<HexCell>().ReturnCoords(), newChessman, chessman.GetComponent<Chessman>().ChessPlayer);
            
            //Destroy(chessman);
            CloseTransformWindow();
        }
        catch (MissingReferenceException) {
            Debug.Log(chessman);
            Debug.Log(newChessman);
        }
    }

    private void CloseTransformWindow() {
        GameManager.Instance.AbilityIsReady();
        transformWindow.SetActive(false);
    }
}
