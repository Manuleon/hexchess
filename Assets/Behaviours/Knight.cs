﻿using System.Collections.Generic;
using UnityEngine;

public class Knight : Movable {

    //private delegate void ChangeIndexes();
    //private delegate void CheckMoves(ChangeIndexes fn);

    //var board = BoardManager.Instance;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    //Knight

    public override Dictionary<HexCell, HexGrid.MoveType> GetPossibleMoves(int currentX, int currentY, int currentZ, int maxValue, Player player) {
        var board = BoardManager.Instance;
        int indexX = 0;
        int indexY = 0;
        int indexZ = 0;
        var possibleMoves = new Dictionary<HexCell, HexGrid.MoveType>();
        HexCell cell;

        //we have 3 directions, and we need to check all of them
        CheckMoves checkMoves = changeCoords => {
            //for each direction changes 2 coords, and one is const
            indexX = currentX;
            indexY = currentY;
            indexZ = currentZ;



             //we'll stop after findind another chessman or the limit of the board
                changeCoords(); //for example indexY++; indexZ--;
            if (indexX > maxValue || indexX < 0 || indexY > maxValue || indexY < 0 || indexZ < 0 || indexZ > maxValue)
                return;

                cell = board.GetCellByCoords(indexX, indexY, indexZ);

                if (cell != null) {
                    //if cell has other's player chessman - ATTACK
                    if (cell.HasChessman()) {

                        if (cell.GetChessman().GetComponent<Chessman>().ChessPlayer == null)
                            Debug.Log("ChessPlayer null");

                        if (cell.GetChessman().GetComponent<Chessman>().ChessPlayer != player) {
                            possibleMoves.Add(cell, HexGrid.MoveType.Kill);
                        }
                    return;
                    }
                    //if cell is empty - you can move
                    else
                        possibleMoves.Add(cell, HexGrid.MoveType.Move);
                }
            ;
        };


        checkMoves(() => { indexX -= 2; indexY -= 1; indexZ += 3;});
        checkMoves(() => { indexX -= 1; indexY -= 2; indexZ += 3; });

        checkMoves(() => { indexX += 2; indexY -= 3; indexZ += 1; });
        checkMoves(() => { indexX += 1; indexY -= 3; indexZ += 2; });

        checkMoves(() => { indexX += 3; indexY -= 2; indexZ -= 1; });
        checkMoves(() => { indexX += 3; indexY -= 1; indexZ -= 2; });

        checkMoves(() => { indexX += 1; indexY += 2; indexZ -= 3; });
        checkMoves(() => { indexX += 2; indexY += 1; indexZ -= 3; });

        checkMoves(() => { indexX -= 1; indexY += 3; indexZ -= 2; });
        checkMoves(() => { indexX -= 2; indexY += 3; indexZ -= 1; });

        checkMoves(() => { indexX -= 3; indexY += 2; indexZ += 1; });
        checkMoves(() => { indexX -= 3; indexY += 1; indexZ += 2; });


        ////X direction
        //checkMoves(() => { indexZ++; indexY--; });
        //checkMoves(() => { indexZ--; indexY++; });

        ////Y direction
        //checkMoves(() => { indexX++; indexZ--; });
        //checkMoves(() => { indexX--; indexZ++; });
        ////Z direction
        //checkMoves(() => { indexY++; indexX--; });
        //checkMoves(() => { indexY--; indexX++; });

        return possibleMoves;
    }
}

