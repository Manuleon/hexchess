﻿using UnityEngine;
using System.Collections;

public class Player {
    public Material mat;
    private string _playerName;
    private Color _textColor;
    private int _score;
    public Fraction fraction;

    public string PlayerName {
        get { return _playerName; }
        set { _playerName = value;}
    }

    public int CurrentScore {
        get { return _score; }
        set { _score = value; }
    }

    public Color TextColor {
        get { return _textColor; }
        set { _textColor = value; }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
