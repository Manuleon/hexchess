﻿using System.Collections.Generic;
using UnityEngine;

public class Griffin : Movable {

    //contains types of moves:
    //Move -- we can only move on the cell (if available)
    //Kill -- we can only attack the cell (if there is an enemy there)
    private enum TurnTypes { Move, Kill };
    //indicates the current turn type
    private TurnTypes _turnType;

    public override Dictionary<HexCell, HexGrid.MoveType> GetPossibleMoves(int currentX, int currentY, int currentZ, int boardMaxCoord, Player player) {
        base.DefineCoords(currentX, currentY, currentZ);
        base.DefineBoardSize(boardMaxCoord);
        var board = BoardManager.Instance;

        Dictionary<ChangeIndexes, TurnTypes> args = new Dictionary<ChangeIndexes, TurnTypes>();

        args.Add(() => { CurrentX -= 3; CurrentZ += 3; }, TurnTypes.Kill);
        args.Add(() => { CurrentX -= 3; CurrentY += 1; CurrentZ += 2; }, TurnTypes.Kill);
        args.Add(() => { CurrentX -= 3; CurrentY += 2; CurrentZ += 1; }, TurnTypes.Kill);
        args.Add(() => { CurrentX -= 3; CurrentY += 3; }, TurnTypes.Kill);
        args.Add(() => { CurrentX -= 1; CurrentY += 3; CurrentZ -= 2; }, TurnTypes.Kill);
        args.Add(() => { CurrentX -= 2; CurrentY += 3; CurrentZ -= 1; }, TurnTypes.Kill);
        args.Add(() => { CurrentY += 3; CurrentZ -= 3; }, TurnTypes.Kill);
        args.Add(() => { CurrentX += 1; CurrentY += 2; CurrentZ -= 3; }, TurnTypes.Kill);
        args.Add(() => { CurrentX += 2; CurrentY += 1; CurrentZ -= 3; }, TurnTypes.Kill);
        args.Add(() => { CurrentX += 3; CurrentZ -= 3; }, TurnTypes.Kill);
        args.Add(() => { CurrentX += 3; CurrentY -= 1; CurrentZ -= 2; }, TurnTypes.Kill);
        args.Add(() => { CurrentX += 3; CurrentY -= 2; CurrentZ -= 1; }, TurnTypes.Kill);
        args.Add(() => { CurrentX += 3; CurrentY -= 3; }, TurnTypes.Kill);
        args.Add(() => { CurrentX += 2; CurrentY -= 3; CurrentZ += 1; }, TurnTypes.Kill);
        args.Add(() => { CurrentX += 1; CurrentY -= 3; CurrentZ += 2; }, TurnTypes.Kill);
        args.Add(() => { CurrentZ += 3; CurrentY -= 3; }, TurnTypes.Kill);
        args.Add(() => { CurrentX -= 1; CurrentY -= 2; CurrentZ += 3; }, TurnTypes.Kill);
        args.Add(() => { CurrentX -= 2; CurrentY -= 1; CurrentZ += 3; }, TurnTypes.Kill);

        var possibleMoves = new Dictionary<HexCell, HexGrid.MoveType>();
        HexCell cell;

        foreach (var arg in args) {
            CurrentX = currentX;
            CurrentY = currentY;
            CurrentZ = currentZ;

            arg.Key();

            _turnType = arg.Value;

            if (CurrentX > maxSize || CurrentX < 0 || CurrentY > maxSize || CurrentY < 0 || CurrentZ < 0 || CurrentZ > maxSize) {
                //Debug.Log("no available moves");
            }
            else {
                cell = board.GetCellByCoords(CurrentX, CurrentY, CurrentZ);
                if (cell != null) {
                    if (cell.HasChessman()) {
                        if (cell.GetChessman().GetComponent<Chessman>().ChessPlayer != player && _turnType == TurnTypes.Kill) {
                            possibleMoves.Add(cell, HexGrid.MoveType.Kill);
                        }
                    }
                    else if (_turnType == TurnTypes.Move || _turnType == TurnTypes.Kill) {
                        possibleMoves.Add(cell, HexGrid.MoveType.Move);
                    }
                }
            }
        };

        return possibleMoves;
    }
}

