﻿using UnityEngine;

public class Chessman : MonoBehaviour {


    private Player _player;
    public int value; //the cost of our chessman!
    public string name;

    public Player ChessPlayer
    {
        get{ return _player;  }
        set{ _player = value; } 
    }

    void Awake() {
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Move() {

    }
}
