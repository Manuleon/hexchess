﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IslandShaker : MonoBehaviour {

    private Transform islandTransform;
    private Transform chessmanTranform;
    private Vector3 islandStartPos;
    private Vector3 chessmanStartPos;
    private Transform chessmanStartParent;

    private bool moves = false;

    public float amplitude = 6f;

    private IEnumerator ShakeObjects(GameObject chessman)
    {
        islandTransform = transform;
        chessmanTranform = chessman.transform;

        Vector3 speedPerFrame = new Vector3(0f, 5.0f, 0f);

        //запоминаем стартовую высоту
        islandStartPos = islandTransform.position;
        chessmanStartPos = chessman.transform.position;
        chessmanStartParent = chessman.transform.parent;

        //высчитываем максимальный предел вверху и внизу
        float maxYPos = islandStartPos.y + amplitude / 2f;
        float minYPos = islandStartPos.y - amplitude / 2f;

        //логическая переменная для движения вверх и вниз?
        bool up = true;

        //дрейфуем их вверх-вниз
        //пока не дискарднут выделения
        //если это произойдёт - возращаем стартовую высоту острову, а фигуре - только если она на острове
        while (moves)
        {
            if (up)
            {
                islandTransform.position += speedPerFrame * Time.deltaTime;
                chessmanTranform.position += speedPerFrame * Time.deltaTime;
                if (islandTransform.position.y > maxYPos) up = false;
            }
            else
            {
                islandTransform.position -= speedPerFrame * Time.deltaTime;
                chessmanTranform.position -= speedPerFrame * Time.deltaTime;
                if (islandTransform.position.y < minYPos) up = true;
            }
            yield return new WaitForFixedUpdate();
        }
        yield return null;
    }

    public void StopShivering()
    {
        moves = false;
        islandTransform.position = islandStartPos;
        if (chessmanTranform.parent == chessmanStartParent) chessmanTranform.position = chessmanStartPos;
    }

    public void StartShivering(GameObject chessman)
    {
        moves = true;
        StartCoroutine(ShakeObjects(chessman));
    }
}
