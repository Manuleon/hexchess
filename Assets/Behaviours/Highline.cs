﻿using System;
using System.Collections;
using UnityEngine;

public class Highline : MonoBehaviour {

    public float autoSeconds = 0.25f;
    public float minAlpha = 0f;
    public float maxAlpha = 0.2f;

    private float _step;
    private bool _appears;
    private bool _fades;

    private Renderer _rend;

    Material mat1;
    Material mat2;
    Color _color;

    private bool _isActive = false;
    private Vector2 startTextureOffset1 = new Vector2(0.3f, 0.05f);
    private Vector2 startTextureOffset2 = new Vector2(0.9f, 0.05f);


    void Awake() {
        _rend = GetComponentInChildren<Renderer>();
        mat1 = _rend.materials[0];
        mat2 = _rend.materials[1];
        mat1.SetTextureOffset("_MainTex", startTextureOffset1);
        mat2.SetTextureOffset("_MainTex", startTextureOffset2);
    }

    // Update is called once per frame
    void Update () { 
        if (_fades) {
            _color = mat1.GetColor("_TintColor");//Tint Color
            _color.a -= _step * Time.deltaTime;
            mat1.SetColor("_TintColor", _color);
            _color = mat2.GetColor("_TintColor");//Tint Color
            _color.a -= _step * Time.deltaTime;
            mat2.SetColor("_TintColor", _color);
        }
        if (_appears) {
            _color = mat1.GetColor("_TintColor");
            _color.a += _step * Time.deltaTime;
            mat1.SetColor("_TintColor", _color);

            _color = mat2.GetColor("_TintColor");
            _color.a += _step * Time.deltaTime;
            mat2.SetColor("_TintColor", _color);
        }
        if (_isActive) {
            Vector2 tiling = mat1.GetTextureOffset("_MainTex");
            if (tiling.x >= 1f) mat1.SetTextureOffset("_MainTex", startTextureOffset1);
            else {
                tiling.x += 0.001f;
                mat1.SetTextureOffset("_MainTex", tiling);
            }

            tiling = mat2.GetTextureOffset("_MainTex");
            if (tiling.x <= 0f) mat2.SetTextureOffset("_MainTex", startTextureOffset2);
            else {
                tiling.x -= 0.001f;
                mat2.SetTextureOffset("_MainTex", tiling);
            }
        }
	}

    public void Fade() {
        //we don't need to fade twice
        if (!_rend.enabled) return;
        Fade(autoSeconds);
    }

    public void Appear() {
        Appear(autoSeconds);
    }

    /*
    maxAlpha - minAlpha = общее расстояние смены прозрачности
    / seconds = шаг за фрейм
    */
    public void Fade(float seconds) {
        _step = (maxAlpha - minAlpha) / seconds;
        //альфа = максимум
        _fades = true;
        StartCoroutine(StartFade(seconds));
    }

    public void Appear(float seconds) {
        _isActive = true;
        _step = (maxAlpha - minAlpha) / seconds;
        //Debug.Log("step = " + _step);

        //альфа = мпинимум
        _color = mat1.GetColor("_TintColor");
        _color.a = minAlpha;
        mat1.SetColor("_TintColor", _color);
        _color = mat2.GetColor("_TintColor");
        _color.a = minAlpha;
        mat2.SetColor("_TintColor", _color);
        _appears = true;
        _rend.enabled = true;
        StartCoroutine(StartAppear(seconds));
    }

    private IEnumerator StartFade(float seconds) {
        _appears = false;
        yield return new WaitForSeconds(seconds);
        _fades = false;
        _isActive = false;
        if (!_appears) {
            _rend.enabled = false;
        }
        yield break;
    }

    //active means with active chessman
    private IEnumerator StartAppear(float seconds) {
        _fades = false;
        yield return new WaitForSeconds(seconds);
        _appears = false;
        yield break;
    }

    public void ChangeColors(Color color1, Color color2)
    {
        Color alpha = color1;
        //alpha.a = 0.5f;
        mat1.SetColor("_TintColor", color1);
        alpha = color2;
        //alpha.a = 0.5f;
        mat2.SetColor("_TintColor", color2);
    }
}
