﻿using System.Collections.Generic;
using UnityEngine;

public class King : Movable {

    //var board = BoardManager.Instance;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public override Dictionary<HexCell, HexGrid.MoveType> GetPossibleMoves(int currentX, int currentY, int currentZ, int boardMaxCoord, Player player) {
        base.DefineCoords(currentX, currentY, currentZ);
        base.DefineBoardSize(boardMaxCoord);

        List<ChangeIndexes> args = new List<ChangeIndexes>();

        args.Add(() => { CurrentY--; CurrentZ++; });
        args.Add(() => { CurrentY++; CurrentZ--; });
        args.Add(() => { CurrentX--; CurrentZ++; });
        args.Add(() => { CurrentX++; CurrentZ--; });
        args.Add(() => { CurrentY++; CurrentX--; });
        args.Add(() => { CurrentY--; CurrentX++; });

        args.Add(() => { CurrentX += 2; CurrentY--; CurrentZ--; });
        args.Add(() => { CurrentZ += 2; CurrentY--; CurrentX--; });
        args.Add(() => { CurrentY += 2; CurrentX--; CurrentZ--; });
        args.Add(() => { CurrentX -= 2; CurrentY++; CurrentZ++; });
        args.Add(() => { CurrentZ -= 2; CurrentY++; CurrentX++; });
        args.Add(() => { CurrentY -= 2; CurrentX++; CurrentZ++; });


        base.DefineIndexexChanging(args);

        Dictionary<HexCell, HexGrid.MoveType> defaultMoves = FindMoves(1, player);
        return defaultMoves;
    }

    public Dictionary<HexCell, HexGrid.MoveType> FindMoves(int distance, Player player) {
        var board = BoardManager.Instance;
        int startX = CurrentX;
        int startY = CurrentY;
        int startZ = CurrentZ;
        var possibleMoves = new Dictionary<HexCell, HexGrid.MoveType>();
        HexCell cell;

        //we have 3 directions, and we need to check all of them
        CheckMoves checkMoves = changeCoords => {
            //for each direction changes 2 coords, and one is const
            CurrentX = startX;
            CurrentY = startY;
            CurrentZ = startZ;

            for (int i = 0; i < distance; i++) { //we'll stop after findind another chessman or the limit of the board
                changeCoords(); //for example CurrentX++; CurrentZ--;
                if (startX > maxSize || startX < 0 || CurrentY > maxSize || CurrentY < 0 || CurrentZ < 0 || CurrentZ > maxSize) break;

                cell = board.GetCellByCoords(CurrentX, CurrentY, CurrentZ);

                if (cell != null) {
                    //if cell has other's player chessman - ATTACK
                    if (cell.HasChessman()) {
                        if (cell.GetChessman().GetComponent<Chessman>().ChessPlayer != player) {
                            possibleMoves.Add(cell, HexGrid.MoveType.Kill);
                        }
                        break;
                    }
                    //if cell is empty - you can move
                    else possibleMoves.Add(cell, HexGrid.MoveType.Move);
                }
            };
        };

        foreach (var fn in fnIndexChange) {
            checkMoves(fn);
        }

        return possibleMoves;
    }
}
