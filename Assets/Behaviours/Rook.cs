﻿using System.Collections.Generic;
using UnityEngine;

public class Rook : Movable {

    //var board = BoardManager.Instance;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override Dictionary<HexCell, HexGrid.MoveType> GetPossibleMoves(int currentX, int currentY, int currentZ, int boardMaxCoord, Player player) {
        base.DefineCoords(currentX, currentY, currentZ);
        base.DefineBoardSize(boardMaxCoord);

        List<ChangeIndexes> args = new List<ChangeIndexes>();

        args.Add(() => { CurrentY--; CurrentZ++; });
        args.Add(() => { CurrentY++; CurrentZ--; });
        args.Add(() => { CurrentX--; CurrentZ++; });
        args.Add(() => { CurrentX++; CurrentZ--; });
        args.Add(() => { CurrentY++; CurrentX--; });
        args.Add(() => { CurrentY--; CurrentX++; });

        base.DefineIndexexChanging(args);

        return base.FindMoves(player);
    }
}
