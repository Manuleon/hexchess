﻿using UnityEngine;
using System.Collections.Generic;

public class Movable : MonoBehaviour {
    protected int CurrentX { get; set; }
    protected int CurrentY { get; set; }
    protected int CurrentZ { get; set; }

    

    public bool _firstMove = true;
    /// <summary>
    /// When we make first move - sign private variable _firstMove to false
    /// </summary>
    public void SignFirstMove()
    {
        _firstMove = false;
    }

    protected int maxSize;

    protected delegate void ChangeIndexes();
    protected delegate void CheckMoves(ChangeIndexes fn);

    protected List<ChangeIndexes> fnIndexChange;

    public bool isFirstMove = true;

    public void SetPosition(int x, int y, int z) {
        CurrentX = x;
        CurrentY = y;
        CurrentZ = z;
    }

    //in inherite scripts you MUST declare x, y, z, board limit, how we change indexes (List<ChangeIndexes>)
    public virtual Dictionary<HexCell, HexGrid.MoveType> GetPossibleMoves(int currentX, int currentY, int currentZ, int boardMaxCoord, Player player) {
        return FindMoves(player);
    }

    protected void DefineCoords(int currentX, int currentY, int currentZ) {
        CurrentX = currentX;
        CurrentY = currentY;
        CurrentZ = currentZ;
    }

    protected void DefineBoardSize(int maxValue) {
        maxSize = maxValue;
    }

    protected void DefineIndexexChanging(List<ChangeIndexes> _fnIndexChange) {
        fnIndexChange = _fnIndexChange;
    }

    public virtual Dictionary<HexCell, HexGrid.MoveType> FindMoves(Player checkForPlayer) {
        var board = BoardManager.Instance;
        int startX = CurrentX;
        int startY = CurrentY;
        int startZ = CurrentZ;
        var possibleMoves = new Dictionary<HexCell, HexGrid.MoveType>();
        HexCell cell;

        //we have 3 directions, and we need to check all of them
        CheckMoves checkMoves = changeCoords => {
            //for each direction changes 2 coords, and one is const
            CurrentX = startX;
            CurrentY = startY;
            CurrentZ = startZ;

            while (true) { //we'll stop after findind another chessman or the limit of the board
                changeCoords(); //for example CurrentX++; CurrentZ--;
                if (startX > maxSize || startX < 0 || CurrentY > maxSize || CurrentY < 0 || CurrentZ < 0 || CurrentZ > maxSize) break;

                cell = board.GetCellByCoords(CurrentX, CurrentY, CurrentZ);

                if (cell != null) {
                    //if cell has other's player chessman - ATTACK
                    if (cell.HasChessman()) {
                        if (cell.GetChessman().GetComponent<Chessman>().ChessPlayer != checkForPlayer) {
                            possibleMoves.Add(cell, HexGrid.MoveType.Kill);
                        }
                        break;
                    }
                    //if cell is empty - you can move
                    else possibleMoves.Add(cell, HexGrid.MoveType.Move);
                }
            };
        };

        if (fnIndexChange != null)
            foreach (var fn in fnIndexChange) {
                checkMoves(fn);
            }

        return possibleMoves;
    }

    public virtual Dictionary<HexCell, HexGrid.MoveType> FindMoves(int distance) {
        var board = BoardManager.Instance;
        int startX = CurrentX;
        int startY = CurrentY;
        int startZ = CurrentZ;
        var possibleMoves = new Dictionary<HexCell, HexGrid.MoveType>();
        HexCell cell;

        //we have 3 directions, and we need to check all of them
        CheckMoves checkMoves = changeCoords => {
            //for each direction changes 2 coords, and one is const
            CurrentX = startX;
            CurrentY = startY;
            CurrentZ = startZ;

            for (int i = 0; i < distance; i++) { //we'll stop after findind another chessman or the limit of the board
                changeCoords(); //for example CurrentX++; CurrentZ--;
                if (startX > maxSize || startX < 0 || CurrentY > maxSize || CurrentY < 0 || CurrentZ < 0 || CurrentZ > maxSize) break;

                cell = board.GetCellByCoords(CurrentX, CurrentY, CurrentZ);

                if (cell != null) {
                    //if cell has other's player chessman - ATTACK
                    if (cell.HasChessman()) {
                        if (cell.GetChessman().GetComponent<Chessman>().ChessPlayer != board.CurrentPlayer) {
                            possibleMoves.Add(cell, HexGrid.MoveType.Kill);
                        }
                        break;
                    }
                    //if cell is empty - you can move
                    else possibleMoves.Add(cell, HexGrid.MoveType.Move);
                }
            };
        };

        foreach (var fn in fnIndexChange) {
            checkMoves(fn);
        }

        return possibleMoves;
    }

    void Awake() {
        maxSize = HexGrid.boardSize;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
