﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;
using System;

public class BoardManager : MonoBehaviour {
    public static BoardManager Instance { get; set; }

    public Movable[] Chessmans { get; set; }                        //Array with chesses on board.
    private Movable selectedChessman;                                //Current selected chessman.

    public Material whitePlayerColor;
    public Material blackPlayerColor;
    public Material angelsPlayerColor;
    public Material undeadPlayerColor;

    public bool[] allowedMoves { get; set; }

    //public Server server;
    public GameObject rookPrefab;
    public GameObject queenPrefab;
    public GameObject knightPrefab;
    public GameObject kingPrefab;
    public GameObject pawnPrefab;
    public GameObject bishopPrefab;
    public GameObject griffinPrefab;
    public GameObject cuirassierPrefab;
    public GameObject mummyPrefab;
    public GameObject skeletBowmanPrefab;

    public GameObject horseManPrefab;

    [SerializeField]
    private GameObject UIManager;
    private TextManager _textManager;

    public Player GetFirstPlayer() {
        return _firstPlayer;
    }

    public Player GetSecondPlayer() {
        return _secondPlayer;
    }

    private Vector3 _centerPoint;

    private Movable _kingType;

    private int selectionX = -1;                                      //Current selection on X axis.
    private int selectionY = -1;                                      //Current selection on Y axis.

    private Player _currentPlayer;
    public Player CurrentPlayer
    {
        get { return _currentPlayer; }
    }

    public Dictionary<Player, HexCell> kings;

    private Player _firstPlayer = new Player();
    private Player _secondPlayer = new Player();

    public UnityEvent changeCameraFn;

    [SerializeField]
    private GameObject CameraGO;
    private CameraController _cameraManager;

    [SerializeField]
    private GameObject ScoreLabel1;
    [SerializeField]
    private GameObject ScoreLabel2;

    private FractionInfo fractionInfo;

    void ChangeCamera() {
        changeCameraFn.Invoke();
    }

    public UnityEvent checkAfterPlayerChange;
    public UnityEvent gameManagerEndTurn;

    private void InitializePlayers()
    {
        _textManager = UIManager.GetComponent<TextManager>();
        Debug.Log("InitializePlayers");
        _currentPlayer = _firstPlayer;

        fractionInfo = new FractionInfo();
        _firstPlayer.fraction = fractionInfo.fractionsList[0];
        _secondPlayer.fraction = fractionInfo.fractionsList[1];

        _firstPlayer.PlayerName = "WHITE LAWFUL GOOD PLAYER";
        _secondPlayer.PlayerName = "EVIL DARK PLAYER";
        _firstPlayer.mat = _firstPlayer.fraction.mat;
        _secondPlayer.mat = _secondPlayer.fraction.mat;
        _firstPlayer.TextColor = Color.white;
        _secondPlayer.TextColor = Color.white;
        
        _textManager.NextTurnText(_firstPlayer);
    }

    public void AddScore(Player player, int score) {
        player.CurrentScore += score;
        Debug.Log("Player: " + player.PlayerName + " Score: " + player.CurrentScore);
    }

    public void SetCenterPoint(Vector3 point) {
        _centerPoint = point;
    }

    public Vector3 GetCenterPoint() {
        return _centerPoint;
    }

    private void InitializePositions() {
        kings = new Dictionary<Player, HexCell>();

        Debug.Log("InitializePositions");
        //player stores "fraction" - Fraction fraction
        //now gots from FractionList from FractionInfo

        List<Placement> firstPlayerPlacement = new List<Placement>();
        List<Placement> secondPlayerPlacement = new List<Placement>();
        foreach (Fraction fractionEntry in fractionInfo.fractionsList) {
            if (fractionEntry == _firstPlayer.fraction) {
                fractionInfo.fractionChessmen.TryGetValue(fractionEntry.TypeName, out firstPlayerPlacement);
            }
            if (fractionEntry == _secondPlayer.fraction) {
                fractionInfo.fractionChessmen.TryGetValue(fractionEntry.TypeName, out secondPlayerPlacement);
            }
        }

        foreach (Placement place in firstPlayerPlacement) {
            InitializeChesses(place.FirstPlayerVector(), place.ChessmanPrefab(), _firstPlayer);
        }

        foreach (Placement place in secondPlayerPlacement) {
            InitializeChesses(place.SecondPlayerVector(), place.ChessmanPrefab(), _secondPlayer);
        }

        if (!kings.ContainsKey(_firstPlayer)) Debug.LogError("Add a king for the first player!");
        if (!kings.ContainsKey(_secondPlayer)) Debug.LogError("Add a king for the second player!");
    }

    void Awake() {
        Instance = this;
        _cameraManager = CameraGO.GetComponent<CameraController>();
        InitializePlayers();
        _kingType = kingPrefab.GetComponent<Movable>();
        
    }

    // Use this for initialization
    void Start () {
        InitializePositions();
        InitializeAbilities();
    }
	
	// Update is called once per frame
	void Update () {
 
    }

    private void InitializeAbilities() {
        CheckTransform checkAbility = new CheckTransform();
        List<Player> firstPlayerList = new List<Player>() { _firstPlayer };
        InitializeAbility(new Vector3(9, 0, 6), checkAbility, firstPlayerList);
        InitializeAbility(new Vector3(5, 0, 10), checkAbility, firstPlayerList);
        InitializeAbility(new Vector3(7, 0, 8), checkAbility, firstPlayerList);
        InitializeAbility(new Vector3(6, 0, 9), checkAbility, firstPlayerList);
        InitializeAbility(new Vector3(8, 0, 7), checkAbility, firstPlayerList);
        InitializeAbility(new Vector3(10, 0, 5), checkAbility, firstPlayerList);
        InitializeAbility(new Vector3(10, 1, 4), checkAbility, firstPlayerList);
        InitializeAbility(new Vector3(10, 2, 3), checkAbility, firstPlayerList);
        InitializeAbility(new Vector3(10, 3, 2), checkAbility, firstPlayerList);
        InitializeAbility(new Vector3(10, 4, 1), checkAbility, firstPlayerList);
        InitializeAbility(new Vector3(10, 5, 0), checkAbility, firstPlayerList);

        List<Player> secondPlayerList = new List<Player>() { _secondPlayer };
        InitializeAbility(new Vector3(0, 5, 10), checkAbility, secondPlayerList);
        InitializeAbility(new Vector3(0, 6, 9), checkAbility, secondPlayerList);
        InitializeAbility(new Vector3(0, 7, 8), checkAbility, secondPlayerList);
        InitializeAbility(new Vector3(0, 8, 7), checkAbility, secondPlayerList);
        InitializeAbility(new Vector3(0, 9, 6), checkAbility, secondPlayerList);
        InitializeAbility(new Vector3(0, 10, 5), checkAbility, secondPlayerList);
        InitializeAbility(new Vector3(1, 10, 4), checkAbility, secondPlayerList);
        InitializeAbility(new Vector3(2, 10, 3), checkAbility, secondPlayerList);
        InitializeAbility(new Vector3(3, 10, 2), checkAbility, secondPlayerList);
        InitializeAbility(new Vector3(4, 10, 1), checkAbility, secondPlayerList);
        InitializeAbility(new Vector3(5, 10, 0), checkAbility, secondPlayerList);
    }

    private void InitializeAbility(Vector3 coords, HexAbility ability, List<Player> nonWorkPlayers) {
        GetCellByCoords((int)coords.x, (int)coords.y, (int)coords.z).AddAbility(ability, nonWorkPlayers);
    }

    public void InitializeChesses(Vector3 pos, GameObject chessman, Player player) {
        GameObject currentChessman = chessman;
        Chessman chessScript = currentChessman.GetComponent<Chessman>();
        if (chessScript == null)
            chessScript = currentChessman.AddComponent<Chessman>();

        Movable[] types = currentChessman.GetComponents<Movable>();

        Renderer[] materials = currentChessman.GetComponentsInChildren<Renderer>();

        foreach (var mat in materials)
            mat.material = player.mat;
            
        if (!SpawnChessmanOnCell(pos, chessman, player))
            Debug.LogError("Can't find cell with position " + pos);
    }

    public HexCell GetCellByCoords(int x, int y, int z) {
        for (int i = 0; i < HexGrid.cellsCount; i++) {
            HexCell currentCell = HexGrid.cells[i];
            if (currentCell.X == x && currentCell.Y == y && currentCell.Z == z) {
                return currentCell;
            }
        }
        return null;
    }

    private HexCell FindChessmanCell(Vector3 coords) {
        for (int i = 0; i < HexGrid.cellsCount; i++) {
            HexCell currentCell = HexGrid.cells[i];
            if (currentCell.X == coords.x && currentCell.Y == coords.y && currentCell.Z == coords.z) {
                return currentCell;
            };
        };
        return null;
        }

    public bool SpawnChessmanOnCell(Vector3 coords, GameObject chessType, Player player) {
        HexCell cell = FindChessmanCell(coords);

        if (cell != null) {
            //check for no more than 1 king
            if (chessType == player.fraction.kingPrefab) {
                if (kings.ContainsKey(player)) {
                    Debug.LogError("You shouldn't add TWO kings! I will not add the king on the cell (" + cell.X + " " + cell.Y + " " + cell.Z + ")");
                    return false;
                }
                else {
                    kings.Add(player, cell);
                }
            }
                
            cell.SpawnChessman(chessType, player);
            return true;
        }
        else return false;
    }

    public void NextPlayerTurn() {
        _cameraManager.SaveCameraDistanceTransform();
        if (_firstPlayer == _currentPlayer) _currentPlayer = _secondPlayer;
        else _currentPlayer = _firstPlayer;
        _textManager.NextTurnText(_currentPlayer);
        _cameraManager.SetCameraTransform();
        checkAfterPlayerChange.Invoke();
        //server.CmdStep("Player end turn");
        gameManagerEndTurn.Invoke();
    }

    internal bool IsKing(Chessman chessman)
    {
        return (CurrentPlayer.fraction.kingPrefab.GetComponentInChildren<Movable>().GetType() == chessman.GetComponentInChildren<Movable>().GetType());
    }

    
}