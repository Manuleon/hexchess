﻿using UnityEngine;
using System.Collections;

public class HexileModel : MonoBehaviour {

    public static int angle = 0;

    void OnEnable() {
        //0 60 120 180 240 300 <- 6 elements
        //int num = Random.Range(0, 5);
        //transform.Rotate(Vector3.up, num * 60f);

        if (angle < 6) {
            angle++;
        }
        else angle = 0;

        transform.Rotate(Vector3.up, angle * 60f);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
