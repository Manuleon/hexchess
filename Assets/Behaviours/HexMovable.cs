﻿using UnityEngine;
using System.Collections.Generic;
using System;

//в обраточике хранится также, на каких игроков распостраняется "абилка", а на каких нет

public abstract class HexAbility {
    List<Player> notAffectedPlayers;

    public void SetPlayersExceptionsList(List<Player> players) {
        notAffectedPlayers = players;
    }

    public abstract void Move(HexCell startCell, Chessman chessman, HexCell endCell);

    protected bool AffectToPlayer(Player player) {
        if (notAffectedPlayers != null) return true;
        foreach (var currentPlayer in notAffectedPlayers)
            if (player == currentPlayer) return false;
        return true;
    }

    public abstract bool WorksFor(GameObject chessman);
}

public class StandartMove : HexAbility {
    public override void Move(HexCell startCell, Chessman chessman, HexCell endPoint) {
        endPoint.SetCurrentChessman(startCell.GetChessman());

        startCell.GetChessman().transform.parent = endPoint.gameObject.transform;
        startCell.GetChessman().transform.localPosition = - new Vector3(0f, endPoint.yOffset, 0f);
        startCell.SetCurrentChessman(null);

        GameManager.Instance.AbilityIsReady();
    }

    Transform lastChessmanParent;
    GameObject lastChessman;
    GameObject lastEnemyChessman;

    public void ChangeParent(HexCell startPoint, Transform endPoint) {
        GameObject chessman = startPoint.GetChessman();
        lastEnemyChessman = endPoint.GetComponent<HexCell>().GetChessman();
        lastChessman = chessman;
        lastChessmanParent = chessman.transform.parent;
        chessman.transform.SetParent(endPoint, true);
        endPoint.GetComponent<HexCell>().SetCurrentChessman(chessman);
        startPoint.GetComponent<HexCell>().SetCurrentChessman(null);
    }

    public void ReturnParent() {
        Transform prevParent = lastChessman.transform.parent;
        lastChessman.transform.parent = lastChessmanParent;
        prevParent.GetComponent<HexCell>().SetCurrentChessman(lastEnemyChessman);
        lastChessmanParent.GetComponent<HexCell>().SetCurrentChessman(lastChessman);
    }

    public override bool WorksFor(GameObject chessman) {
        if (chessman.GetComponent<Chessman>()) return true;
        return false;
    }
}

public class CheckTransform : HexAbility {
    public override void Move(HexCell startCell, Chessman chessman, HexCell endCell) {
        CheckingTransform(chessman);
    }

    private void CheckingTransform(Chessman chessman) {
        if (AffectToPlayer(chessman.ChessPlayer)) {
            MinionTransforms transforms = chessman.gameObject.GetComponent<MinionTransforms>();
            if (transforms != null) {
                GameManager.Instance.CallTransformWindow(chessman.gameObject, transforms);
            }
        };
    }

    public override bool WorksFor(GameObject chessman) {
        if (AffectToPlayer(chessman.GetComponent<Chessman>().ChessPlayer) && chessman.GetComponent<MinionTransforms>()) return true;
        return false;
    }
}