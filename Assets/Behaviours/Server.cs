﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Server : NetworkBehaviour {

    public NetworkManager manager;
    [SerializeField]
   // private HexGrid hexGrid;
    //public NetworkView view;
    private void Start()
    {
        if (Network.isServer)
        {
            if (manager == null)
            {
                manager = GetComponent<NetworkManager>();
            }
            /*if(view == null)
            {
                view = GetComponent<NetworkView>();
                view.stateSynchronization = NetworkStateSynchronization.Off;
                view.observed = this;
            } */
            manager.maxConnections = 2;
            GameObject serverManager = GameObject.Find("ServerManager");
            serverManager.active = true;
        }


        //Debbuging
        //InitializedServer();
    }


    public void InitializedServer()
    {
        Network.InitializeServer(manager.maxConnections, manager.networkPort, false);
    }
    
    [Command]    
    public void CmdStep(int moveType, Vector3 start, Vector3 final)
    {
        Debug.Log("Server: MoveType = "+ moveType + " " + start + " -> "+final);
        RpcStep(moveType, start, final);
    }
    [ClientRpc]
    public void RpcStep(int moveType, Vector3 start, Vector3 final)
    {
        HexGrid hexGrid = GameObject.Find("HexGrid").GetComponent<HexGrid>();
        HexGrid.MoveType type = (HexGrid.MoveType) moveType;
        hexGrid.ActivateChessman(type, start, final);

    }
}