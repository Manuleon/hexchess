﻿using System.Collections.Generic;
using UnityEngine;

public class SkeletonBowman : Movable {

    //contains types of moves:
    private enum TurnTypes { Move, Arrow };
    //indicates the current turn type
    private TurnTypes _turnType;

    public override Dictionary<HexCell, HexGrid.MoveType> GetPossibleMoves(int currentX, int currentY, int currentZ, int boardMaxCoord, Player player) {
        base.DefineCoords(currentX, currentY, currentZ);
        base.DefineBoardSize(boardMaxCoord);
        var board = BoardManager.Instance;

        Dictionary<ChangeIndexes, TurnTypes> args = new Dictionary<ChangeIndexes, TurnTypes>();

        //for first player
        if (board.CurrentPlayer == board.GetFirstPlayer()) {
            args.Add(() => { CurrentX -= 1; CurrentZ += 1; }, TurnTypes.Arrow);
            args.Add(() => { CurrentY += 1; CurrentZ -= 1; }, TurnTypes.Arrow);
            args.Add(() => { CurrentY += 1; CurrentX -= 1; }, TurnTypes.Move);
        }
        //for another
        else {
            args.Add(() => { CurrentY -= 1; CurrentZ += 1; }, TurnTypes.Arrow);
            args.Add(() => { CurrentX += 1; CurrentZ -= 1; }, TurnTypes.Arrow);
            args.Add(() => { CurrentY -= 1; CurrentX += 1; }, TurnTypes.Move);
        }

        var possibleMoves = new Dictionary<HexCell, HexGrid.MoveType>();
        HexCell cell;

        foreach (var arg in args) {
            CurrentX = currentX;
            CurrentY = currentY;
            CurrentZ = currentZ;

            arg.Key();

            _turnType = arg.Value;

            if (CurrentX > maxSize || CurrentX < 0 || CurrentY > maxSize || CurrentY < 0 || CurrentZ < 0 || CurrentZ > maxSize) {
                Debug.Log("no available moves");
            }
            else {
                cell = board.GetCellByCoords(CurrentX, CurrentY, CurrentZ);
                if (cell != null) {
                    if (cell.HasChessman()) {
                        if (cell.GetChessman().GetComponent<Chessman>().ChessPlayer != player && _turnType == TurnTypes.Arrow) {
                            possibleMoves.Add(cell, HexGrid.MoveType.Arrow);
                        }
                    }
                    else if (_turnType == TurnTypes.Move) {
                        possibleMoves.Add(cell, HexGrid.MoveType.Move);
                    }
                }
            }
        };

        return possibleMoves;
    }

}
