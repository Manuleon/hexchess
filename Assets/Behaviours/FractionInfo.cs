﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum FractionType {Magic, Normal};
public enum FractionName { WhiteNormal, BlackNormal, Undead, Angels };

public class FractionInfo {
    public Fraction[] fractionsList;

    public Dictionary<FractionName, List<Placement>> fractionChessmen;

    public FractionInfo() {
        fractionChessmen = new Dictionary<FractionName, List<Placement>>();
        BoardManager board = BoardManager.Instance;

        fractionsList = new Fraction[] {
            new Fraction(FractionType.Normal, FractionName.WhiteNormal, board.whitePlayerColor, board.kingPrefab),
            new Fraction(FractionType.Normal, FractionName.BlackNormal, board.blackPlayerColor, board.kingPrefab),
            new Fraction(FractionType.Magic, FractionName.Angels, board.angelsPlayerColor, board.kingPrefab),
            new Fraction(FractionType.Magic, FractionName.Undead, board.undeadPlayerColor, board.mummyPrefab)
        };

        GameObject rook = board.rookPrefab;

        List<Placement> normalPlacement = new List<Placement>();

        normalPlacement.Add(new Placement( new Point(7, 0, 8), new Point(0, 7, 8), rook));
        normalPlacement.Add(new Placement( new Point(10, 3, 2), new Point(3, 10, 2), rook));

        /*GameObject bishop = board.bishopPrefab;
        normalPlacement.Add(new Placement(new Point(8, 2, 5), new Point(0, 10, 5), bishop));
        normalPlacement.Add(new Placement(new Point(9, 1, 5), new Point(1, 9, 5), bishop));
        normalPlacement.Add(new Placement(new Point(10, 0, 5), new Point(2, 8, 5), bishop));

        GameObject knight = board.knightPrefab;
        normalPlacement.Add(new Placement(new Point(8, 0, 7), new Point(0, 8, 7), knight));
        normalPlacement.Add(new Placement(new Point(10, 2, 3), new Point(2, 10, 3), knight));
        */
        GameObject king = board.kingPrefab;
        normalPlacement.Add(new Placement(new Point(10, 1, 4), new Point(1, 10, 4), king));

        /*GameObject queen = board.queenPrefab;
        normalPlacement.Add(new Placement(new Point(9, 0, 6), new Point(0, 9, 6), queen));*/
        GameObject pawn = board.pawnPrefab;

        /*normalPlacement.Add(new Placement(new Point(6, 0, 9), new Point(0, 6, 9), pawn));
        normalPlacement.Add(new Placement(new Point(6, 1, 8), new Point(1, 6, 8), pawn));
        normalPlacement.Add(new Placement(new Point(6, 2, 7), new Point(2, 6, 7), pawn));
        normalPlacement.Add(new Placement(new Point(6, 3, 6), new Point(3, 6, 6), pawn));
        normalPlacement.Add(new Placement(new Point(6, 4, 5), new Point(4, 6, 5), pawn));
        normalPlacement.Add(new Placement(new Point(7, 4, 4), new Point(4, 7, 4), pawn));
        normalPlacement.Add(new Placement(new Point(8, 4, 3), new Point(4, 8, 3), pawn));
        normalPlacement.Add(new Placement(new Point(9, 4, 2), new Point(4, 9, 2), pawn));
        normalPlacement.Add(new Placement(new Point(10, 4, 1), new Point(4, 10, 1), pawn));*/
        //normalPlacement.Add(new Placement(new Point(3, 9, 3), new Point(9, 3, 3), pawn));

        fractionChessmen.Add(FractionName.WhiteNormal, normalPlacement);
        fractionChessmen.Add(FractionName.BlackNormal, normalPlacement);

        List<Placement> angelsPlacement = new List<Placement>();

        angelsPlacement.Add(new Placement(new Point(7, 0, 8), new Point(0, 7, 8), rook));
        angelsPlacement.Add(new Placement(new Point(10, 3, 2), new Point(3, 10, 2), rook));

        angelsPlacement.Add(new Placement(new Point(10, 1, 4), new Point(1, 10, 4), king));

        //angelsPlacement.Add(new Placement(new Point(8, 2, 5), new Point(0, 10, 5), bishop));
        //angelsPlacement.Add(new Placement(new Point(9, 1, 5), new Point(1, 9, 5), bishop));
        //angelsPlacement.Add(new Placement(new Point(10, 0, 5), new Point(2, 8, 5), bishop));

        GameObject horseMan = board.horseManPrefab;
        angelsPlacement.Add(new Placement(new Point(9, 4, 2), new Point(4, 9, 2), horseMan));
        angelsPlacement.Add(new Placement(new Point(6, 1, 8), new Point(1, 6, 8), horseMan));

        GameObject griffin = board.griffinPrefab;
        angelsPlacement.Add(new Placement(new Point(8, 0, 7), new Point(0, 8, 7), griffin));
        angelsPlacement.Add(new Placement(new Point(10, 2, 3), new Point(2, 10, 3), griffin));

        GameObject cuirassier = board.cuirassierPrefab;
        angelsPlacement.Add(new Placement(new Point(6, 2, 7), new Point(2, 6, 7), cuirassier));
        angelsPlacement.Add(new Placement(new Point(6, 3, 6), new Point(3, 6, 6), cuirassier));
        angelsPlacement.Add(new Placement(new Point(6, 4, 5), new Point(4, 6, 5), cuirassier));
        angelsPlacement.Add(new Placement(new Point(7, 4, 4), new Point(4, 7, 4), cuirassier));
        angelsPlacement.Add(new Placement(new Point(8, 4, 3), new Point(4, 8, 3), cuirassier));

        fractionChessmen.Add(FractionName.Angels, angelsPlacement);

        List<Placement> nekroPlacement = new List<Placement>();

        //nekroPlacement.Add(new Placement(new Point(8, 2, 5), new Point(0, 10, 5), bishop));
        //nekroPlacement.Add(new Placement(new Point(9, 1, 5), new Point(1, 9, 5), bishop));
        //nekroPlacement.Add(new Placement(new Point(10, 0, 5), new Point(2, 8, 5), bishop));

        GameObject skeletBowman = board.skeletBowmanPrefab;
        nekroPlacement.Add(new Placement(new Point(6, 2, 7), new Point(2, 6, 7), skeletBowman));
        nekroPlacement.Add(new Placement(new Point(6, 3, 6), new Point(3, 6, 6), skeletBowman));
        nekroPlacement.Add(new Placement(new Point(6, 4, 5), new Point(4, 6, 5), skeletBowman));
        nekroPlacement.Add(new Placement(new Point(7, 4, 4), new Point(4, 7, 4), skeletBowman));
        nekroPlacement.Add(new Placement(new Point(8, 4, 3), new Point(4, 8, 3), skeletBowman));

        GameObject mummy = board.mummyPrefab;
        nekroPlacement.Add(new Placement(new Point(10, 1, 4), new Point(1, 10, 4), mummy));

        fractionChessmen.Add(FractionName.Undead, nekroPlacement);
    }
}



public class Fraction {
    public GameObject kingPrefab;

    private FractionType _type;
    public FractionType Type {
        get { return _type; }
        set { _type = value; }
    }

    private FractionName _typeName;
    public FractionName TypeName {
        get { return _typeName; }
        set { _typeName = value; }
    }

    public Material mat;

    public Fraction(FractionType type, FractionName fractionName, Material material, GameObject king) {
        _type = type;
        _typeName = fractionName;
        mat = material;
        kingPrefab = king;
    }
}

public class Placement {
    private Point _firstPlayerPlace;
    private Point _secondPlayerPlace;
    private GameObject _prefab;

    public Placement(Point point1, Point point2, GameObject chessman) {
        _firstPlayerPlace = point1;
        _secondPlayerPlace = point2;
        _prefab = chessman;
    }

    public Vector3 FirstPlayerVector() {
        return _firstPlayerPlace.VectorPoint();
    }

    public Vector3 SecondPlayerVector() {
        return _secondPlayerPlace.VectorPoint();
    }

    public GameObject ChessmanPrefab() {
        return _prefab;
    }
}

public struct Point {
    private int _X;
    private int _Y;
    private int _Z;

    public Point(int x, int y, int z) {
        _X = x;
        _Y = y;
        _Z = z;
    }

    public Vector3 VectorPoint() {
        return new Vector3(_X, _Y, _Z);
    }
}