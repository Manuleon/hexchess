﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public static UIManager Instance { get; set; }

    [SerializeField]
    private GameObject classicGameButton;
    [SerializeField]
    private GameObject magicGameButton;
    [SerializeField]
    private GameObject optionsButton;
    [SerializeField]
    private GameObject exitGameButton;

    void Awake() {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartClassicGame() {
        LoadByIndex(1);
    }

    public void StartMagicGame() {

    }

    public void ShowOptions() {

    }

    public void Quit() {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    public void LoadByIndex(int sceneIndex) {
        SceneManager.LoadScene(sceneIndex);
    }

    public void LoadByName(string name) {
        SceneManager.LoadScene(name);
    }
}
