﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabItem : MonoBehaviour
{
    [SerializeField]
    HexGrid hexGrid;
    // 1
    private GameObject collidingObject;
    // 2
    private GameObject objectInHand;
    private SteamVR_TrackedObject trackedObj;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }
    
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Update is called once per frame
    void Update()
    {
        Ray raycast = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        // 1
        if (Controller.GetHairTriggerDown())
        {

            if (Physics.Raycast(raycast, out hit))
            {
                switch (Controller.index) {
                case 3:
                    hexGrid.ClickCell(hit.point);
                    break;
                case 4:
                    hexGrid.DiscardSelection();
                    break;
                }
                
            }
        }

        //Ray raycast = new Ray(transform.position, transform.forward);
        //RaycastHit hit;
        //// 1
        //if (Controller.GetHairTriggerDown())
        //{
        //    if (Physics.Raycast(raycast, out hit))
        //    {
        //        hexGrid.ClickCell(hit.point);
        //    }
        //}
        //
        //// 2
        //if (Controller.GetHairTriggerUp())
        //{
        //    if (Physics.Raycast(raycast, out hit))
        //    {
        //        hexGrid.DiscardSelection();
        //    }
        //}
    }

}

