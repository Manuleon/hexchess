﻿using System.Collections.Generic;
using UnityEngine;

public class HexCell : MonoBehaviour {
    private HexCoordinates _coordinates;
    private Vector3 _offsetCoordinates = Vector3.zero;
    public float yOffset;

    public Color _currentColor;
    public Color _nativeColor;
    private GameObject _currentChessman;

    public static int sizeLimit;

    public List<HexAbility> hexAbilities = new List<HexAbility>();

    private int _x;
    public int X {
        get { return _x + HexGrid.boardSize - 1; }
        set { _x = value; }
    }
    private int _y;
    public int Y {
        get { return _y + HexGrid.boardSize - 1; }
        set { _y = value; }
    }
    private int _z;
    public int Z {
        get { return _z + HexGrid.boardSize - 1; }
        set { _z = value; }
    }

    void Awake() {
        hexAbilities.Add(new StandartMove());
    }

    public Vector3 ReturnCoords() {
        return new Vector3(X, Y, Z);
    }

    private Vector3 GetOffsetCoords() {
        if (_offsetCoordinates == Vector3.zero) _offsetCoordinates = new Vector3(0f, -yOffset, 0f);
        return _offsetCoordinates;
    }

    public void AddAbility(HexAbility ability, List<Player> playerExceptions) {
        ability.SetPlayersExceptionsList(playerExceptions);
        hexAbilities.Add(ability);
    }

    public void SetNativeColor(Color color) {
        _nativeColor = color;
        _currentColor = _nativeColor;
    }

    public void SetSelectionColor(Color color) {
        _currentColor = color;
    }

    public void SetCurrentChessman(GameObject chessman) {
        _currentChessman = chessman;
    }

    public void UndoSelection() {
        _currentColor = _nativeColor;
    }

    public Color GetCurrentColor() {
        return _currentColor;
    }

    public void SpawnChessman(GameObject chessman, Player player) {
        _currentChessman = Instantiate(chessman) as GameObject;
        _currentChessman.transform.SetParent(transform);
        _currentChessman.transform.localPosition = GetOffsetCoords();
        //_currentChessman.transform.localPosition = new Vector3(0f, -yOffset, 0f);
        _currentChessman.transform.rotation = chessman.transform.rotation;
        _currentChessman.GetComponentInChildren<Chessman>().ChessPlayer = player;
    }

    public void RemoveChessman() {
        if (_currentChessman != null) Destroy(_currentChessman);
        else {
            Debug.LogError("You're trying to remove a chessman, but this cell doesn't have one!");
        }
    }

    public bool HasChessman() {
        return (_currentChessman != null);
    }

    public GameObject GetChessman()
    {
        return _currentChessman;
    }

    public Dictionary<HexCell, HexGrid.MoveType> GetChessmanMoves() {
        //function allows a player to see active chessman and shows him possible moves
        //we'll return possible moves (cells)

        if (_currentChessman == null) {
            Debug.Log("There is any chessmen on those cell!");
            return null;
        }

        Movable[] movableScripts = _currentChessman.GetComponentsInChildren<Movable>();
        if (movableScripts == null) {
            Debug.LogError("Your chessmans don't have movable scripts!");
            return null;
        }

        var board = BoardManager.Instance;
        var possibleMoves = new Dictionary<HexCell, HexGrid.MoveType>();

        sizeLimit = (HexGrid.boardSize - 1) * 2;
        foreach (Movable script in movableScripts) {
            Dictionary<HexCell, HexGrid.MoveType> currentPossibleMoves = script.GetPossibleMoves(X, Y, Z, sizeLimit, board.CurrentPlayer);
            foreach (var pair in currentPossibleMoves) {
                possibleMoves.Add(pair.Key, pair.Value);
            }
        }

        return possibleMoves;
    }


    void Update() {

    }
}


    