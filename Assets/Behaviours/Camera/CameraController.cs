﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class CameraController : MonoBehaviour {

    public int cameraMinDistance = 0;
    public int cameraMaxDistance = 120;

    public float cameraRotatingSpeed = 40f;
    public float cameraMoovingSpeed = 10f;

    private int _сameraCurrentDistance { get; set; }

    private Vector3 _cameraPlayer1Pos;
    private Vector3 _cameraPlayer2Pos = new Vector3(8f, 100f, 240f);
    private Quaternion _cameraPlayer1Rotate;
    private Quaternion _cameraPlayer2Rotate = new Quaternion(0f, -0.9f, 0.4f, 0f);

    private int _cameraPlayer1Distance = 0;
    private int _cameraPlayer2Distance = 0;

    // Use this for initialization
    void Start () {
        _сameraCurrentDistance = 0;
        _cameraPlayer1Rotate = transform.rotation;
        _cameraPlayer2Rotate = _cameraPlayer2Rotate;
        _cameraPlayer1Pos = transform.position;
        _cameraPlayer2Pos = _cameraPlayer2Pos;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButton(1)) {
            ChangeCameraDistance();
            ChangeCameraRotation();
        }
    }

    private void ChangeCameraDistance() {
        float mouseScroll = Input.GetAxis("Mouse Y");
        
        if (mouseScroll != 0) {
            if ((mouseScroll > 0 && _сameraCurrentDistance + 1 < cameraMaxDistance) ||
            (mouseScroll < 0 && _сameraCurrentDistance - 1 > cameraMinDistance)) {
                transform.Translate(Vector3.forward * cameraMoovingSpeed * mouseScroll);
                _сameraCurrentDistance += (int)(mouseScroll * 10);
            }
        }
    }

    private void ChangeCameraRotation() {
        float mouseRotation = Input.GetAxis("Mouse X");

        if (mouseRotation != 0) {
            transform.RotateAround(new Vector3(0, 0, 95), Vector3.up, cameraRotatingSpeed * mouseRotation * Time.deltaTime);
        }
    }

    public void SetCameraTransform() {
        
        if (BoardManager.Instance.CurrentPlayer == BoardManager.Instance.GetFirstPlayer()) {
            transform.position = _cameraPlayer1Pos;
            transform.rotation = _cameraPlayer1Rotate;
            _сameraCurrentDistance = _cameraPlayer1Distance;

            if (_сameraCurrentDistance < _cameraPlayer1Distance) {
                while (_сameraCurrentDistance < _cameraPlayer1Distance)
                    _сameraCurrentDistance++;
            }
            else {
                while (_сameraCurrentDistance < _cameraPlayer1Distance)
                    _сameraCurrentDistance--;
            }
        }
        else {
            transform.position = _cameraPlayer2Pos;
            transform.rotation = _cameraPlayer2Rotate;
            _сameraCurrentDistance = _cameraPlayer2Distance;

            if (_сameraCurrentDistance < _cameraPlayer2Distance) {
                while (_сameraCurrentDistance < _cameraPlayer2Distance)
                    _сameraCurrentDistance++;
            }
            else {
                while (_сameraCurrentDistance < _cameraPlayer2Distance)
                    _сameraCurrentDistance--;
            }
        }
        
    }

    public void SaveCameraDistanceTransform() {

        if (BoardManager.Instance.CurrentPlayer == BoardManager.Instance.GetFirstPlayer()) {
            _cameraPlayer1Pos = transform.position;
            _cameraPlayer1Rotate = transform.rotation;
            _cameraPlayer1Distance = _сameraCurrentDistance;
        }
        else {
            _cameraPlayer2Pos = transform.position;
            _cameraPlayer2Rotate = transform.rotation;
            _cameraPlayer2Distance = _сameraCurrentDistance;
        }
        
    }
}
