﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour {

    public static GameManager Instance { get; set; }

    private int _activeAbilitiesCount = 0;

    public GameObject UIManager;
    private TextManager _textManager;

    // Use this for initialization
    void Start () {
        Instance = this;
        _textManager = UIManager.GetComponent<TextManager>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CallTransformWindow (GameObject chessman, MinionTransforms transforms) {
        //we need to take all prefabs of transforms,
        //take all buttons from TextManager
        //every button should return a prefab and we need to set a name of the prefab chessman
        GameObject[] chessmans = transforms.transformPrefabs;
        _textManager.ShowTransformWindow(chessman, chessmans);
    }

    //we are waiting finishing all abilities before we 
    public void SetActiveAbilities(int count) {
        _activeAbilitiesCount = count;
    }

    public void AbilityIsReady() {
        _activeAbilitiesCount--;
        //Debug.Log("_activeAbilitiesCount: " + _activeAbilitiesCount);
        if (_activeAbilitiesCount <= 0) BoardManager.Instance.NextPlayerTurn();
    }

    internal void RevertTurn()
    {
        //do nothing
    }
}
